//
//  SecondViewController.swift
//  delegate
//
//  Created by BTB_011 on 11/4/20.
//

import UIKit

protocol ColorTextDelegate {
    func didColorText(msg: String, color: UIColor, img: UIImage)
}
class SecondViewController: UIViewController {
    var delegate: ColorTextDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func btnUpload(_ sender: UIButton) {
        let icon = UIImage(systemName: "square.and.arrow.up")
        delegate?.didColorText(msg: "Upload", color: sender.currentTitleColor, img: icon!)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnDownload(_ sender: UIButton) {
        let icon = UIImage(systemName: "square.and.arrow.down")
        delegate?.didColorText(msg: "Download...", color: sender.currentTitleColor, img: icon!)
        dismiss(animated: true, completion: nil)
    }
    
}
