//
//  ViewController.swift
//  delegate
//
//  Created by BTB_011 on 11/4/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imgShow: UIImageView!
    @IBOutlet weak var lblShow: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnEvent(_ sender: Any) {
        let secondView = storyboard?.instantiateViewController(withIdentifier: "secondStoryboard") as! SecondViewController
        secondView.delegate = self
        secondView.modalPresentationStyle = .fullScreen
        present(secondView, animated: true, completion: nil)
    }
}

extension ViewController: ColorTextDelegate{
    func didColorText(msg: String, color: UIColor, img: UIImage) {
        self.view.backgroundColor = color
        self.lblShow.text = msg
        imgShow.image = img
    }
}

